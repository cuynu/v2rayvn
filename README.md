# [Switch to English](https://github.com/cuynu/v2rayvn/wiki/English)

# v2rayNG Tiếng Việt / Vietnamese Language

v2rayNG với ngôn ngữ Tiếng Việt! ,hỗ trợ [Xray core](https://github.com/XTLS/Xray-core) và [v2fly core](https://github.com/v2fly/v2ray-core)

Tính năng bổ sung so với phiên bản gốc : Mux (Tăng tốc kết nối, giảm ping nhưng gây áp lực lên máy chủ) và tự động cập nhật gói đăng ký (subscription)

Phiên bản mới nhất : 1.8.25-cuynu

Ngày dựng/cập nhật : (10/03/2024)

Dịch Tiếng Việt bởi : Cuynu, [user09283](https://github.com/user09283)
____________________________________________________

## Tải xuống v2rayNG Tiếng Việt phiên bản mới nhất tại:  

<a href="https://gitlab.com/cuynu/v2rayvn/uploads/111cbef6617ef3a6eec32619c415d934/v2rayNG_1.8.25-cuynu_universal.apk">
<img alt="Tải xuống v2rayNG" src="https://gitlab.com/cuynu/v2rayvn/-/raw/master/pic/1648277008370.png" width="300" height="90" />
</a>

**Từ phiên bản 1.7.4, Bản dịch Tiếng Việt của v2rayNG bởi Cuynu và [user09283](https://github.com/user09283)
 đã có sẵn trên [Google Play](https://play.app.goo.gl/?link=https://play.google.com/store/apps/details?id=com.v2ray.ang&ddl=1&pcampaignid=web_ddl_1) dưới dạng phiên bản chính thức, tuy nhiên bạn vẫn có thể tải xuống APK bên trên nếu bạn không muốn tải xuống từ Google Play.** 

<a href="https://play.app.goo.gl/?link=https://play.google.com/store/apps/details?id=com.v2ray.ang&ddl=1&pcampaignid=web_ddl_1">
<img alt="Get it on Google Play" src="https://play.google.com/intl/vi_vn/badges/images/generic/vi_badge_web_generic.png" width="165" height="64" />
</a>

v2rayNG hiện chỉ có sẵn cho Android và là ứng dụng mã nguồn mở (Open-source), Mã nguồn mở trên repository đã có sẵn.

____________________________________________________

## Mã nguồn / Source code

v2rayNG là ứng dụng "mã nguồn mở", bạn có thể tự tải xuống mã nguồn trên repository và chỉnh sửa bằng Gradle hay Android Studio theo ý của bạn theo giấy phép GPL-3.0.


